package com.example.lambda2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.MessageAttribute;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
//import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSEventRecord;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
//import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;

public class Lambda2 implements RequestHandler<SQSEvent, String> {

	// private static ProfileCredentialsProvider en = new
	// ProfileCredentialsProvider("default");
	private static String awsAccessKeyId = "";
	private static String awsSecretAccessKey = "";
	private static final String regionName = "ca-central-1";
	private String url2 = "";
	private String username2 = ""; // String username2 = "";
	private String password2 = "";
	private static AWSCredentials credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretAccessKey);

	static ClientConfiguration config = new ClientConfiguration();

	private static AmazonSQS client;

	private static AmazonSNS snsClient;

	private AmazonS3 s3;

	private String tableName;// = "Persons";

	@Override
	public String handleRequest(SQSEvent input, Context context) {

		long start = System.currentTimeMillis();

		// try {
		// Thread.sleep(160000);
		// } catch (InterruptedException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }

		System.out.println(System.getenv("ACCESS_KEY_ENV_VAR"));
		System.out.println(System.getenv("SECRET_KEY_ENV_VAR"));
		System.out.println(System.getenv("username1"));
		config.setSocketTimeout(135000);
		config.setClientExecutionTimeout(135000);
		client = AmazonSQSClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(regionName).withClientConfiguration(config).build();
		s3 = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(regionName).withClientConfiguration(config).build();
		snsClient = AmazonSNSClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(regionName).withClientConfiguration(config).build();
		// context.getLogger().log("Input: " + input);
		// String message = input.getRecords().get(0).getBody(); // get the message from
		// the queue

		Map<String, MessageAttribute> map = input.getRecords().get(0).getMessageAttributes();

		// System.out.println("S3Bucket Name "+map.get("bucketName").getStringValue());
		// System.out.print("S3-KeyName"+map.get("bucketKeyName").getStringValue());
		// System.out.println("Message: "+message);
		// workSQS(map.get("bucketName").getStringValue(),map.get("bucketKeyName").getStringValue(),map.get("tableName").getStringValue());
		String prefix = map.get("prefix").getStringValue();
		System.out.println("prefix:" + prefix);
		try {
			// int arr[] = {1,2,3,4,5,6,7,8,9};
			// int wronf_index= arr[10];
			// int i= Integer.parseInt(map.get("bucketName").getStringValue());
			listKeysInDirectory(map.get("bucketName").getStringValue(), prefix); // bucketName
			long totalTime = System.currentTimeMillis() - start;
			System.out.println(totalTime);
			// com.amazonaws.services.sns.model.MessageAttributeValue messageAttributeValue
			// = new com.amazonaws.services.sns.model.MessageAttributeValue()
			// .withDataType("String")
			// .withStringValue("");
			//
			// com.amazonaws.services.lambda.runtime.events.SNSEvent.MessageAttributes.put("",
			// messageAttributeValue);
			//
			// PublishRequest request = new PublishRequest("", "")
			// .withMessageAttributes(messageAttributes);

			PublishRequest publishRequest = new PublishRequest("arn:aws:sns:ca-central-1:******:SuccessTopic",
					"prefix= " + prefix + "\n bucketName =" + map.get("bucketName").getStringValue());
			PublishResult publishResult = snsClient.publish(publishRequest);

			System.out.println(publishResult.getMessageId());
			// MessageAttributeValue messageAttributeValue = new MessageAttributeValue()
			// .withDataType("String")
			// .withStringValue("");
		}

		catch (Exception e) { // ClassNotFoundException | SQLException | IOException
			// TODO Auto-generated catch block

			final SendMessageRequest sendMessageRequest = new SendMessageRequest(
					"https://sqs.ca-central-1.amazonaws.com/*******/DLQDemo", "mssssg" + e.getMessage());
			final Map<String, MessageAttributeValue> messageAttributesNew = new HashMap<String, MessageAttributeValue>();

			messageAttributesNew.put("prefix",
					new MessageAttributeValue().withDataType("String").withStringValue(prefix));

			messageAttributesNew.put("bucketName", new MessageAttributeValue().withDataType("String")
					.withStringValue(map.get("bucketName").getStringValue()));
			sendMessageRequest.setMessageAttributes(messageAttributesNew);
			client.sendMessage(sendMessageRequest);
			System.out.println(e.getStackTrace()); // log it to the cloud watch!!
			e.printStackTrace();

		}

		return "returned";

	}

	// not called anymore

	public void workSQS(String bucket, String buckKey, String table) {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection con2 = DriverManager.getConnection(url2, username2, password2);
			Statement stmt2 = con2.createStatement();
			S3Object response = s3.getObject(new GetObjectRequest(bucket, buckKey));
			String testFileName = buckKey.replace("/", ""); // buckKey+table
			System.out.println("bucket key: " + buckKey);
			File file = new File("/tmp/".concat(testFileName));
			Files.copy(response.getObjectContent(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
			String query = "LOAD DATA LOCAL INFILE '" + file.getPath() + "' INTO TABLE " + table
					+ " FIELDS TERMINATED BY ',' " + "LINES TERMINATED BY '\n' ";
			stmt2.executeUpdate(query);
			FileUtils.forceDelete(file);
			con2.close();
			stmt2.close();
		}

		catch (SQLException | ClassNotFoundException | IOException e) {
			e.printStackTrace();
		} finally {
		}
	}

	// new

	public void listKeysInDirectory(String bucketName, String prefix)
			throws SQLException, ClassNotFoundException, IOException {

		Class.forName("com.mysql.jdbc.Driver");
		Connection con2 = DriverManager.getConnection(url2, username2, password2);
		Statement stmt2 = con2.createStatement();
		String delimiter = "/";
		if (!prefix.endsWith(delimiter)) {
			prefix += delimiter;
		}
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName).withPrefix(prefix)
				.withDelimiter(delimiter);
		ObjectListing objects = s3.listObjects(listObjectsRequest);
		// ObjectListing objects1 = s3.listObjects("lambdabukt","Key/FOUR/");
		// System.out.println("listing objects in objects1 "+objects1);
		// System.out.println("displaying common prefixes of
		// objects1"+objects1.getCommonPrefixes());
		// System.out.println("object summaries: "+objects.getObjectSummaries());
		List<S3ObjectSummary> objectSummary = objects.getObjectSummaries();

		for (S3ObjectSummary summary : objectSummary) {

			String tableName = summary.getKey().replace(prefix, "");
			String key = prefix + tableName;
			String fileName = key.replace("/", "");
			S3Object response = s3.getObject(new GetObjectRequest(bucketName, key));
			File file = new File("/tmp/".concat(fileName));
			// System.out.println(summary.getKey().replace(prefix, ""));
			Files.copy(response.getObjectContent(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
			String query = "LOAD DATA LOCAL INFILE '" + file.getPath() + "' INTO TABLE " + tableName
					+ " FIELDS TERMINATED BY ',' " + "LINES TERMINATED BY '\n' ";
			stmt2.executeUpdate(query);
			FileUtils.forceDelete(file);
		}

		con2.close();
		stmt2.close();
		// objects"+objects.getCommonPrefixes());
	}
}
